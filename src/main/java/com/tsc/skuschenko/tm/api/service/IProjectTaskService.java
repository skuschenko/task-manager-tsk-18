package com.tsc.skuschenko.tm.api.service;

import com.tsc.skuschenko.tm.model.Project;
import com.tsc.skuschenko.tm.model.Task;

import java.util.List;

public interface IProjectTaskService {

    List<Task> findAllTaskByProjectId(String projectId);

    Task bindTaskByProject(String projectId, String taskId);

    Task unbindTaskFromProject(String projectId, String taskId);

    Project deleteProjectById(String projectId);

    void clearProjects();

}
