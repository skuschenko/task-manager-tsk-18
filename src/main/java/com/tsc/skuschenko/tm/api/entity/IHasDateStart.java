package com.tsc.skuschenko.tm.api.entity;

import java.util.Date;

public interface IHasDateStart {

    Date getDateStart();

    void setDateStart(Date dateStart);

}
