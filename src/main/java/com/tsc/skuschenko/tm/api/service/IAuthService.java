package com.tsc.skuschenko.tm.api.service;

import com.tsc.skuschenko.tm.model.User;

public interface IAuthService {

    void login(String login, String password);

    void logout();

    User registry(String login, String password, String email);

    User getUser();

    String getUserId();

    boolean isAuth();

}
