package com.tsc.skuschenko.tm.api.service;

import com.tsc.skuschenko.tm.enumerated.Role;
import com.tsc.skuschenko.tm.model.User;

import java.util.List;

public interface IUserService {

    List<User> findAll();

    User findById(String id);

    User findByLogin(String login);

    User removeById(String id);

    User removeByLogin(String login);

    User findByEmail(String email);

    User create(String login, String password);

    User create(String login, String password, String email);

    User create(String login, String password, Role role);

    User setPassword(String userId, String password);

    User updateUser(
            String userId, String firstName, String lastName, String middleName
    );

    boolean isLoginExist(String login);

    boolean isEmailExist(String login);
}
