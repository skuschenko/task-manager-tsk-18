package com.tsc.skuschenko.tm.api.service;

import com.tsc.skuschenko.tm.command.AbstractCommand;

import java.util.Collection;

public interface ICommandService {

    AbstractCommand getCommandByArg(String name);

    AbstractCommand getCommandByName(String name);

    Collection<AbstractCommand> getCommands();

    Collection<AbstractCommand> getArgs();

    Collection<String> getListArgumentName();

    Collection<String> getListCommandNames();

    void add(AbstractCommand abstractCommand);

}
