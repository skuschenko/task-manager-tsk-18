package com.tsc.skuschenko.tm.exception.empty;

import com.tsc.skuschenko.tm.exception.AbstractException;

public class EmptyEmailException extends AbstractException {

    public EmptyEmailException() {
        super("Error! EMAIL is empty...");
    }

}
