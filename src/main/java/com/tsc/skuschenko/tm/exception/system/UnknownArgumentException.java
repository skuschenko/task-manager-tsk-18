package com.tsc.skuschenko.tm.exception.system;

import com.tsc.skuschenko.tm.exception.AbstractException;

public class UnknownArgumentException extends AbstractException {

    public UnknownArgumentException() {
        super("Error! Argument does not found...");
    }

    public UnknownArgumentException(final String argument) {
        super("Error! Argument '" + argument + "' does not found...");
        System.out.println("Pleas print 'help' for more information");
    }

}
