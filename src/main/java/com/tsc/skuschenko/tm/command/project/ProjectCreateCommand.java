package com.tsc.skuschenko.tm.command.project;

import com.tsc.skuschenko.tm.api.service.IProjectService;
import com.tsc.skuschenko.tm.exception.entity.project.ProjectNotFoundException;
import com.tsc.skuschenko.tm.model.Project;
import com.tsc.skuschenko.tm.util.TerminalUtil;

public class ProjectCreateCommand extends AbstractProjectCommand {

    private final String NAME = "project-create";

    private final String DESCRIPTION = "create new project";

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return NAME;
    }

    @Override
    public String description() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        showOperationInfo(NAME);
        showParameterInfo("name");
        final String name = TerminalUtil.nextLine();
        showParameterInfo("description");
        final String description = TerminalUtil.nextLine();
        final IProjectService projectService
                = serviceLocator.getProjectService();
        final Project project = projectService.add(name, description);
        if (project == null) throw new ProjectNotFoundException();
        showProject(project);
    }

}
