package com.tsc.skuschenko.tm.command.project;

import com.tsc.skuschenko.tm.api.service.IProjectTaskService;

public class ProjectClearCommand extends AbstractProjectCommand {

    private final String NAME = "project-clear";

    private final String DESCRIPTION = "clear all projects";

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return NAME;
    }

    @Override
    public String description() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        showOperationInfo(NAME);
        final IProjectTaskService projectTaskService =
                serviceLocator.getProjectTaskService();
        projectTaskService.clearProjects();
    }

}
