package com.tsc.skuschenko.tm.command.system;

import com.tsc.skuschenko.tm.command.AbstractCommand;

public class VersionShowCommand extends AbstractCommand {

    private final String NAME = "version";

    private final String DESCRIPTION = "version";

    private final String ARGUMENT = "-v";

    @Override
    public String arg() {
        return ARGUMENT;
    }

    @Override
    public String name() {
        return NAME;
    }

    @Override
    public String description() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        showOperationInfo(NAME);
        System.out.println("1.0.0");
    }

}