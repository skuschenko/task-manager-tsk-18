package com.tsc.skuschenko.tm.command.task;

import com.tsc.skuschenko.tm.api.service.ITaskService;
import com.tsc.skuschenko.tm.exception.entity.task.TaskNotFoundException;
import com.tsc.skuschenko.tm.model.Task;
import com.tsc.skuschenko.tm.util.TerminalUtil;

public class TaskChangeStatusByIndexCommand extends AbstractTaskCommand {

    private final String NAME = "task-change-status-by-index";

    private final String DESCRIPTION = "change task by index";

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return NAME;
    }

    @Override
    public String description() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        showOperationInfo(NAME);
        showParameterInfo("index");
        final Integer valueIndex = TerminalUtil.nextNumber() - 1;
        final ITaskService taskService = serviceLocator.getTaskService();
        Task task = taskService.findOneByIndex(valueIndex);
        if (task == null) throw new TaskNotFoundException();
        task = taskService.changeTaskStatusByIndex(
                valueIndex, readTaskStatus()
        );
        if (task == null) throw new TaskNotFoundException();
        showTask(task);
    }

}